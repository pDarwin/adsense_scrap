'use strict';
/**
 * { currently testing for http://www.mandola.in/adsense }
 * data-ad-client="ca-pub-3665752689669427"
  data-ad-slot="1995171792"
 * @refer    http://www.mandola.in/adsense
 */

var http = require('http');
var fs = require('fs');
var path = require('path');
var colors = require('colors');
var clientId = 3665752689669427;
var slotId = 1995171792;
var siteUrl = 'http://www.mandola.in/adsense';
var typeOfAdUnits = ['responsive','728x90','336x280','320x100','300x600'];
try {
	/**
	 make a js/json with async module for calls series
	**/
  var fillHeader = require('./adsdata');
  
  var job = require('./workflow');
  var workflow = new job();

  workflow.on('load google ad js',function() {
  	var options = {
	  "method": "GET",
	  "hostname": "pagead2.googlesyndication.com",
	  "port": 80,
	  "path": "/pagead/js/adsbygoogle.js",
	  "headers": {
	    "Accept": "*/*",
	    "Accept-Encoding":"gzip, deflate, sdch",
	    "Accept-Language":"en-US,en;q=0.8,hi;q=0.6",
	    "Connection": "Keep-alive",
	    "Host": "pagead2.googlesyndication.com",
	    "Referer": "http://www.mandola.in/adsense",
	    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
		  }
		};

		console.log(colors.yellow('load google ad js'));

  	workflow.initiateRequest(options,'ca-pub-3665752689669427.js');
  });


  workflow.on('ca-pub-3665752689669427.js',function() {

  	var options = {
		  "method": "GET",
		  "hostname": "pagead2.googlesyndication.com",
		  "port": 443,
		  "path": "/pub-config/r20160913/ca-pub-3665752689669427.js",
		  "headers": {
		    "Accept": "*/*",
		    "Accept-Encoding":"gzip, deflate, sdch, br",
		    "Accept-Language":"en-US,en;q=0.8,hi;q=0.6",
		    "Connection": "Keep-alive",
		    "Host": "pagead2.googlesyndication.com",
		    "Referer": "http://www.mandola.in/adsense",
		    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
			}
		};
    
    workflow.initiateRequest(options,'zrt_lookup.html');

  });

  workflow.on('zrt_lookup.html',function() {

    var options = {
      "method": "GET",
		  "hostname": "googleads.g.doubleclick.net",
		  "port": 443,
		  "path": "/pagead/html/r20161128/r20161117/zrt_lookup.html",
		  "headers": {
		    "Accept": "*/*",
		    "Accept-Encoding":"gzip, deflate, sdch, br",
		    "Accept-Language":"en-US,en;q=0.8,hi;q=0.6",
		    "Connection": "Keep-alive",
		    "Host": "googleads.g.doubleclick.net",
		    "Referer": "http://www.mandola.in/adsense",
		    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
			}  
    };

    workflow.initiateRequest(options,'show_ads_impl.js');

  });

  workflow.on('show_ads_impl.js',function() {

    var options = {
      "method": "GET",
		  "hostname": "pagead2.googlesyndication.com",
		  "port": 80,
		  "path": "/pagead/js/r20161128/r20161117/show_ads_impl.js",
		  "headers": {
		    "Accept": "*/*",
		    "Accept-Encoding":"gzip, deflate, sdch, br",
		    "Accept-Language":"en-US,en;q=0.8,hi;q=0.6",
		    "Connection": "Keep-alive",
		    "Host": "pagead2.googlesyndication.com",
		    "Referer": "http://www.mandola.in/adsense",
		    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
			}  
    };
    workflow.initiateRequest(options,'osd.js');
  });

  // osd.js and /pagead/ads calls starts from show_ads_impl.js

  workflow.on('osd.js',function() {

    var options = {
      "method": "GET",
		  "hostname": "pagead2.googlesyndication.com",
		  "port": 443,
		  "path": "/pagead/osd.js",
		  "headers": {
		    "Accept": "*/*",
		    "Accept-Encoding":"gzip, deflate, sdch, br",
		    "Accept-Language":"en-US,en;q=0.8,hi;q=0.6",
		    "Connection": "Keep-alive",
		    "Host": "pagead2.googlesyndication.com",
		    "Referer": "http://www.mandola.in/adsense",
		    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
			}  
    };
    workflow.initiateRequest(options,'/pagead/ads?');
  });

  workflow.on('/pagead/ads?',function() {

  	var reqParams = [
      // google_ad_client 
  	  { client : 'ca-pub-'+this.adCallUtils().clientIs },
      // google_ad_format
      { format : '336x280'},
      // google_ad_output
      { output: 'html'},
      // google_ad_height
      { h: 280 },
      // google_ad_slot
      { slotname: this.adCallUtils().slotId},
      // google_ad_unit_key
      { adk: 393407362},
      // google_ad_dom_fingerprint
      { adf: 3025194257 },
      // google_ad_width
      { w: 336 },
      // google_last_modified_time
      { lmt: 1480424165 },
      // google_loeid
      { loeid: 453848102 },
      // google_flash_version
      { flash: '23.0.0' },
      { url: encodeURIComponent(this.adCallUtils().siteUrl ) },
      // google_webgl_support
      { wgl: 1 },
      { dt: new Date().getTime() },
      { bpp: },
      { bdt: },
      { fdt: },
      { idt: },
      { shv: },
      { cbv: },
      { saldr: },
      { correlator: },
      { frm: },
      { ga_vid: },
      { ga_sid: },
      { ga_hid: },
      { ga_fc: 0 },
      { pv: 2 },
      { iag: 3 },
      { icsg: 2 },
      { nhd: 1 },
      { dssz: 2 },
      { mdo: 0 },
      { mso: 0 },
      { u_tz: 330 },
      { u_his: 2 },
      { u_java: 0 },
      { u_h: 800 },
      { u_w: 1280 },
      { u_ah: },
      { u_aw: 1280 },
      { u_cd: 24 },
      // navigator.plugins.length set 5 - user agent is chrome 54
      { u_nplug: 5 },
      // navigator.mimeTypes.length set 7 - user agent is chrome 54
      { u_nmime: 7 },
      { dff: 'times' },
      { dfs: 16 },
      { adx: 8 },
      { ady: 8 },
      { biw: 1280 },
      { bih: 150 },
      { eid: },
      { oid: },
      { rx: },
      { eae: },
      { fc: },
      { pc: },
      { brdim: },
      { vis: },
      { rsz: },
      { abl: },
      { ppjl: },
      { pfx: },
      { fu: },
      // wn = 0;
      //window.SVGElement && document.createElementNS && (wn |= 1);
      { bc: 1 },
      { ifi: },
      { xpc: },
      //  encodeURIComponent(t.document.location.protocol), "//", encodeURIComponent(t.document.location.host)].join("")
      { p: [encodeURIComponent(this.adCallUtils().protocol), "//", encodeURIComponent(this.adCallUtils().host)].join("") },
      // Cp((new Date).getTime(), a)
      { dtd: }
  	];

  	var options = {
      "method": "GET",
		  "hostname": "googleads.g.doubleclick.net",
		  "port": 443,
		  "path": "/pagead/ads?",
		  "headers": {
		    "Accept": "*/*",
		    "Accept-Encoding":"gzip, deflate, sdch, br",
		    "Accept-Language":"en-US,en;q=0.8,hi;q=0.6",
		    "Connection": "Keep-alive",
		    "Host": "googleads.g.doubleclick.net",
		    "Referer": "http://www.mandola.in/adsense",
		    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36"
			}  
    };

    process.nextTick(function(){
      workflow.initiateRequest(options,'last call');
    });

  });

  workflow.on('last call',function() {
  	console.log(colors.green('done'));
  	// 1 is used for failure & 0 for success
  	process.exit(0);
  });

  workflow.on('requestCompleted',function(resHeaders,nextCall) {
    // this event emits next call
    console.log(resHeaders);
    console.log(colors.yellow(nextCall));
    workflow.emit(nextCall); 
  });
 
	workflow.emit('load google ad js');
	/** 
	  to do
	  // website url with phnatom
	*/
}
catch(e) {
	console.log(e);
	// 1 is used for failure & 0 for success
  process.exit(1);
}




