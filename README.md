
--------- ***** install global modules ********** -----------
  - phantom js
  - casper js
  - anyproxy.io 

--------- ****** install node modules *********** ------------  

    package.json

--------- ***** anyproxy.io bugs **************** ------------
  
  anyproxy code not sending accept-encoding : gzip request header and in response deleteing 
  content-encoding header 

   so for testing install anyproxy.io locally and uninstall anyproxy.io global and change code according to request / response     

---------****** casper js calls ****************---------------
phantom js / casper js calls

     node casperServer.js

---------****** node calls match *************** ---------------

   node callsMatch.js

   // here we are just matching calls that browser fires running calls in series  using events

---------***** issues pending ****************** -------------
  
  - casper stacktrace fix
  - plugins /mimes in casper js 
  - html5 APis in casper js
  - callsMatch.js parameters matching pending 
--------- ***** refernces ******************
  // http://engineering.shapesecurity.com/2015/01/detecting-phantomjs-based-visitors.html
  // http://www.slideshare.net/SergeyShekyan/shekyan-zhang-owasp
     
