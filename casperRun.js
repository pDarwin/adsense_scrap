(function(){
  //'use strict';
  /**
   * Created by prao
   * sudo casperjs --ignore-ssl-errors=yes --ssl-protocol=any casper_fix.js
   */
  
  // hide and seek phantomjs / casperjs
  // http://engineering.shapesecurity.com/2015/01/detecting-phantomjs-based-visitors.html
  // http://www.slideshare.net/SergeyShekyan/shekyan-zhang-owasp

  /**
   *  spoof HTTP stack - pending
   *  spoof Client-side User-Agent Check -done
   *  spoof Plugins - improve - mime type
   *  spoof Timing - done
   *  spoof Global Properties window._phantom and window.callPhantom - done
   *  sppof Lack of JavaScript Engine Features - done
   *  spoof Stack Traces - pending
   */


  var casper = require('casper').create({
    clientScripts: [],
    pageSettings: {
      proxy: 'http://localhost:8001',
      loadImages: true,
      loadPlugins: true,
      javascriptEnabled: true,
      webSecurityEnabled: false,
      userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36'
    },
    logLevel: "error",
    verbose: true
  });

  var currentTime = (new Date()).getTime();

  var x = require('casper').selectXPath;
  var links;
  var alinks;


  casper.on('page.initialized', function () {

    // try {

        /**
        * spoof Plugins
        */

        var oldNavigator = navigator;
        var oldPlugins = oldNavigator.plugins;
        var plugins = {};
        plugins.length = 1;
        plugins.__proto__ = oldPlugins.__proto__;


        window.navigator = { plugins: plugins };
        window.navigator.__proto__ = oldNavigator.__proto__;

        window.navigator["cookieEnabled"]= true;
        window.navigator["language"] = "en-US";
        window.navigator["productSub"] = "20030107";
        window.navigator["mimeTypes"] =  {
          "length": 1
        };
        window.navigator["product"] = "Gecko";
        window.navigator["appCodeName"] = "Mozilla";
        window.navigator["vendorSub"] = "";
        window.navigator["vendor"] = "Apple Computer, Inc.";
        window.navigator["platform"] = "MacIntel";
        window.navigator["appName"] = "Netscape";
        window.navigator["appVersion"] = "5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36";
        window.navigator["onLine"] = true;


        /**
         *  spoof client side user agent
         */
        
        window.navigator.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36';
        /**
         * spoof Global Properties window._phantom and window.callPhantom
        */

        var p = window.callPhantom;
        delete window._phantom;
        delete window.callPhantom;

        Object.defineProperty(window, "myCallPhantom", {
          get: function () {
            return p;
          },
          set: function () {
          },
          enumerable: false
        });
      
        /**
        * spoof lack of javascript feature
        * 1. toString
          2. method.call(context, arguments);
          3. method.apply
          4. method.bind
        */
        var indexOfArray = function indexOfArray(a, b) {
            var la = a.length;
            for (var i = 0; i < la; i++) {
                if (a[i] === b) {
                    return i;
                }
            }
            return -1;
        };

        var bound = [];
        var oldCall = Function.prototype.call;
        var oldApply = Function.prototype.apply;
        var slice = [].slice;
        var concat = [].concat;
        oldCall.call = oldCall;
        oldCall.apply = oldApply;
        oldApply.call = oldCall;
        oldApply.apply = oldApply;

        function call() {
            return oldCall.apply(this, arguments);
        }

        Function.prototype.call = call;

        function apply() {
            return oldApply.apply(this, arguments);
        }

        Function.prototype.apply = apply;

        function bind() {
            var func = this;
            var self = arguments[0];
            var rest = oldCall.call(slice.call, slice, arguments, 1);
            rest.concat = concat;
            var result = (function () {
                var args = oldCall.call(slice.call, slice, arguments, 0);
                return func.apply(self, rest.concat(args));
            });
            bound.push(result);
            return result;
        }

        Function.prototype.bind = bind;

        var nativeFunctionString = Error.toString().replace(/Error/g, "bind");
        var nativeToStringFunctionString = Error.toString().replace(/Error/g, "toString");
        var nativeBoundFunctionString = Error.toString().replace(/Error/g, "");
        var nativeCallFunctionString = Error.toString().replace(/Error/g, "call");
        var nativeApplyFunctionString = Error.toString().replace(/Error/g, "apply");
        var oldToString = Function.prototype.toString;

        function functionToString() {
          if (this === bind) {
              return nativeFunctionString;
          }
          if (this === functionToString) {
              return nativeToStringFunctionString;
          }
          if (this === call) {
              return nativeCallFunctionString;
          }
          if (this === apply) {
              return nativeApplyFunctionString;
          }
          var idx = indexOfArray(bound, this);
          if (idx >= 0) {
              return nativeBoundFunctionString;
          }
          return oldCall.call(oldToString, this);
        }

        Function.prototype.toString = functionToString;
    // } catch(e) {
    //   console.log('page.initialized error', e.stack);
    //  }

  });

  // print out all the messages in the headless browser context
  casper.on('remote.message', function (msg) {
    this.echo('remote message caught: ' + msg);
  });

  // print out all the messages in the headless browser context
  casper.on("page.error", function (msg, trace) {
    this.echo("Page Error: " + msg, "ERROR");
  });

  casper.on("remote.alert", function (message) {
    for (var i = 0; i < 1e8; i++) {
    }
    return "a";
  });

  //Emitted when a remote window.callPhantom(data) call has been performed.
  // check this call if browser is calling "window.callPhantom()"
  casper.on("remote.callback", function (data) {
    console.log("window call phantom called");
  });

  casper.on("url.changed", function () {
    this.then(function () {
      this.echo(this.getTitle());
    });
  });

  // casper.on("navigation.requested", function(url, navigationType, navigationLocked, isMainFrame) {
  //     //this.echo(" -------> Navigation requested to :" + url);
  //     require('fs').write("navigation"+currentTime+".json", url, 'w+');
  // });

  // casper.on("url.changed", function(url) {
  //     //this.echo(" -------> Url changed to :" + url);
  //     require('fs').write("urlchanged"+currentTime+".json", url, 'w+');
  // });

  /**
   * creating request logs
   */
  casper.on('resource.requested', function (resource) {
    var url = resource.url,
      method = resource.method,
      headers = resource.headers.reduce(function (a, obj, i) {
        var strings = obj.name + ":" + obj.value;
        a.push(strings + "\n");
        return a;
      }, []);

    var output = "Request URL:" + url + "\n" +
      "Request Method:" + method + "\n" +
      "\n" +
      "\n" +
      "Request Headers" + "\n" +
      "view source" + "\n" +
      headers +
      "\n" +
      "\n" +
      "\n" +
      "------------------------" + "\n";

    require('fs').write("request" + currentTime + ".text", output, 'w+');
  });

  /**
   * creating request response logs
   */
  casper.on('resource.received', function (resource) {

    if (resource.stage == 'end') {
      var url = resource.url,
        headers = resource.headers.reduce(function (a, obj, i) {
          var strings = obj.name + ":" + obj.value;
          a.push(strings + "\n");
          return a;
        }, []);

      var output = "Request URL:" + url + "\n" +
        "\n" +
        "\n" +
        "Response Headers" + "\n" +
        "view source" + "\n" +
        headers +
        "\n" +
        "\n" +
        "\n" +
        "------------------------" + "\n";
      require('fs').write("resource2" + currentTime + ".text", output, 'w+');
    }
  });

  casper.start('http://www.mandola.in/adsense');

  casper.then(function () {  
    // console.log('Stack from a non-thrown error', new Error().stack);
    // var e = new Error();
    // console.log('Stack from a not-yet thrown error (e)', e.stack);

    // try {
    //   throw e;
    // } catch(e) {
    //   console.log('Stack from the now-thrown error (e)', e.stack);
    // }

    /**
     * testing spoof Global Properties window._phantom and window.callPhantom
     */
    if (window.callPhantom || window._phantom) {
      console.log("callPhantom PhantomJS environment detected.");
    }
    else {
      console.log("callPhantom PhantomJS environment not detected.");
    }

    /**
     *  testing plugin spoof
     */
    if (!(navigator.plugins instanceof window.PluginArray) || navigator.plugins.length === 0) {
      console.log("PhantomJS environment detected.");
    }
    else {
      console.log("PhantomJS environment not detected.");
    }

    /**
     *  testing client side user agent
     */

    if (/PhantomJS/.test(window.navigator.userAgent)) {
      window.navigator.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36';
    }

  });


  casper.then(function () {
    console.log(this.getCurrentUrl());
    console.log('wait for rendering images');
  });

  casper.wait(20000, function () {
    // console.log('intialised callphantom');
    // window.myCallPhantom();
    console.log("wait complete");
    casper.capture('casper' + (new Date()).getTime() + '.png');
  });

  casper.then(function () {
    require('fs').write("aacaspernew" + (new Date()).getTime() + ".htm", this.getHTML(), 'w');
  });


  // casper.withFrame("google_ads_iframe_/7176/TOI/TOI_World/TOI_World_Pakistan/TOI_ROS_ATF_WLD_PAk_300_0phantomjs", function() {
  //     require('fs').write("casperframeindex"+(new Date()).getTime()+".htm",  this.getHTML(), 'w');         
  // });

  casper.then(function () {
    console.log("page soure");

    if (casper.exists(x('//*[@id="aswift_0"]'))) {
      console.log('exits xpath');
    }
    else {
      console.log('nott xpath');
    }
  });


  casper.thenClick(x('//*[@id="aswift_0"]'), function () {
    console.log("Woop!");
    casper.capture('casper-click' + (new Date()).getTime() + '.png');
  });

  casper.then(function () {
    console.log("wait complete");
    console.log(this.getCurrentUrl());
    casper.capture('casper-after click' + (new Date()).getTime() + '.png');
  });

  // casper.wait(50000, function () {
  //   console.log('------ inspecting socket calls or settimeout calls------');
  // });

  casper.run();

})();