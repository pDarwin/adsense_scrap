/**
 * Created by janruls1 on 07-10-2016.
 * update by prao
 */

'use strict';
var Http = require('http');
var fs = require('fs');
var md5 = require('md5');
var path = require('path');
try {
  var HttpsProxyAgent =  require('./httpsProxyAgent');
  var fillHeader = require('./fillHeader');
  var job = require('./workflow');
  var emitter = new job();
  var reqHeaderOrder = ['accept','accept-encoding','accept-language','host','connection'
  ,'upgrade-insecure-requests','user-agent'];

  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };
}
catch(e) {
  console.log(e);
  // 1 is used for failure & 0 for success
  process.exit(1);
}


module.exports = {
    summary:function(){
        return "this is a rule file for DL_click_minors by zen and prao";
    },

    shouldUseLocalResponse : function(req,reqBody){
        var body_file = __dirname + "/cache/" + md5(req.headers.host + req.url) + "_" + req.headers['dl_cache_proxy_country'] + "_" + req.headers['dl_cache_proxy_device_type'] + "_" + req.headers['dl_cache_proxy_device'] + "_" + req.headers['dl_cache_proxy_browser'] + "_" + "body";
        var header_file = __dirname + "/cache/" + md5(req.headers.host + req.url) + "_" + req.headers['dl_cache_proxy_country'] + "_" + req.headers['dl_cache_proxy_device_type'] + "_" + req.headers['dl_cache_proxy_device'] + "_" + req.headers['dl_cache_proxy_browser'] + "_" + "header";

        if(req.headers.host == "m.facebook.com" || req.headers.host == "facebook.com" || req.headers.host == "www.facebook.com" || req.headers.host.indexOf("fbcdn.") != -1)
            return true;

        if(req.headers['dl_cache_proxy_base_url']== req.url)
            return false;

        var excludeFilesContaining = ['.jpg','.jpeg','.png','.gif','.mp4','.css'];

        if (/\.(png|gif|jpg|jpeg|mp4|css)$/.test(req.url)){
            req.replaceLocalFile = true;
            return true;
        } 
               

        try {
            fs.accessSync(header_file, fs.F_OK);
            fs.accessSync(body_file, fs.F_OK);
            return true;
        } catch (e) {
           return false;
        }

        return false;
    },

    dealLocalResponse : function(req,reqBody,callback){
        var body_file = __dirname + "/cache/" + md5(req.headers.host + req.url) + "_" + req.headers['dl_cache_proxy_country'] + "_" + req.headers['dl_cache_proxy_device_type'] + "_" + req.headers['dl_cache_proxy_device'] + "_" + req.headers['dl_cache_proxy_browser'] + "_" + "body";
        var header_file = __dirname + "/cache/" + md5(req.headers.host + req.url) + "_" + req.headers['dl_cache_proxy_country'] + "_" + req.headers['dl_cache_proxy_device_type'] + "_" + req.headers['dl_cache_proxy_device'] + "_" + req.headers['dl_cache_proxy_browser'] + "_" + "header";

        var excludeFilesContaining = ['.jpg','.jpeg','.png','.gif','.mp4','.css'];

        var is_image = false;

        if(/\.(png|gif|jpg|jpeg|mp4|css)$/.test(req.url)) {
            is_image = true;
            callback(200, {"dl_cm_from" : "dl_local_cache"}, new Buffer("Hi! This is media"));
        }

        if(req.headers.host == "m.facebook.com" || req.headers.host == "facebook.com" || req.headers.host == "www.facebook.com" || req.headers.host.indexOf("fbcdn.") != -1)
            callback(200, {"dl_cm_from" : "dl_local_cache"}, new Buffer("<html><head><meta name='referrer' content='origin' /></head><body>There is nothing see here!</body></html>"));
        else if(!is_image){

            try {
                var body_content = fs.readFileSync(body_file);
                var header_content = JSON.parse(fs.readFileSync(header_file));
                header_content.dl_cm_from = "dl_local_cache";
                if (header_content['content-type'].indexOf("image") == -1 && header_content['content-type'].indexOf("media") == -1)
                    body_content = new Buffer(body_content);
                callback(200, header_content, body_content);
            } catch (err) {
                callback(200, {"error": "cache error"}, new Buffer("Cache loading failed"));
            }
        }

    },

    replaceRequestProtocol:function(req,protocol){
        return protocol;
    },

    replaceRequestOption : function(req,option){

      var protocol = "http";
      var newReq  = (JSON.parse(JSON.stringify(option.headers)));

      if (option.port == 443)
          protocol = "https";


        // if(protocol == "http"){
        //     var url = protocol+"://"+option.hostname+":"+option.port+option.path;
        //     option.hostname = "localhost";
        //     option.port = parseInt(option.headers.dl_cache_proxy_port);
        //     option.path  = url;
        // }
        // else {
        //     var port = parseInt(option.headers.dl_cache_proxy_port);
        //     if(isNaN(port))
        //         port = 24042;
        //     var agent = new HttpsProxyAgent({
        //         proxyHost: 'localhost',
        //         proxyPort: parseInt(option.headers.dl_cache_proxy_port),
        //         rejectUnauthorized : false,
        //     });
        //     option.agent = agent;

        // }
        

        //var newHeaders = {};
        // for (var header in option.headers) {
        //    if(header.indexOf("dl_cache_proxy") == -1) {
        //        newHeaders[header] = option.headers[header];
        //    }

        // }
               

        // use once because we are requiring workflow on top
        
        emitter.once('change order of headers',function() {  

          var newOrderArr = [];

          for (var headers in newReq) {

            var newObj = {};
            newObj[headers] = newReq[headers];

            if(reqHeaderOrder.indexOf(headers) != -1) {
              newOrderArr.insert(reqHeaderOrder.indexOf(headers),newObj);
            }
            else {
              newOrderArr.push(newObj);
            }
          };

          var upReqOrder =  newOrderArr.reduce(function(o,ao,i){
              o[Object.keys(ao).join()] =   ao[Object.keys(ao).join()];
              return o;
          },{});

          emitter.emit('change header values',upReqOrder);

        });

        emitter.once('change header values',function(upReqOrder) {
          
          try {
            for (var headers in upReqOrder) {

              switch(headers) {
                case 'accept-encoding':
                    upReqOrder['accept-encoding'] = 'gzip, deflate, sdch'; 
                    break;
                case 'accept-language':
                    upReqOrder['accept-language'] = 'en-US,en;q=0.8,hi;q=0.6'; 
                    break;  
                case 'connection':
                    upReqOrder.connection = 'keep-alive'; 
                    break;              
                default: 
              }

            }

            if (req.url.indexOf('zrt_lookup.html') != -1 ) 
                upReqOrder['upgrade-insecure-requests'] = 1 ;

            option.headers = upReqOrder;   
          }
          catch(e) {
            console.log(e);             
          }

          emitter.emit('change url params');

        });

        emitter.once('change url params',function(){
            
          try { 

            var hostnameBasePath = option.hostname+(option.path.indexOf('?') > -1 ? option.path.split('?')[0] : option.path);
            
            switch(hostnameBasePath) {

              case 'googleads.g.doubleclick.net/pagead/ads':

                let url = option.path.split('?')[1].split('&');
                  
                var modUrl = url.reduce(function(a,v,i) {
                              
                    let _nameVal = v.split('=')[0];

                    if (!!_nameVal) {

                      switch(_nameVal) {
                         
                        case 'wgl' :
                          v =  v.replace(/\=\w+/,'='+1);
                        break;
                        case 'u_his' :
                          v = v.replace(/\=\w+/,'='+2);
                        break;
                        case 'u_nplug' :
                          v = v.replace(/\=\w+/,'='+5);
                        break;
                        case 'u_nmime' :
                          v = v.replace(/\=\w+/,'='+7);
                        break;
                        case 'u_w' :
                          v = v.replace(/\=\w+/,'='+1280);
                        break;
                        case 'u_aw' :
                          v = v.replace(/\=\w+/,'='+1280);
                        break;
                        case 'u_cd' :
                          v = v.replace(/\=\w+/,'='+24);
                        break;
                        case 'u_h' :
                          v = v.replace(/\=\w+/,'='+800);
                        break;
                        case 'u_ah' :
                          v = v.replace(/\=\w+/,'='+746);
                        break;
                        case 'fc' :
                          v = v.replace(/\=\w+/,'='+80);
                        break;

                        default :

                      }
                    }

                    a.push(v);

                    return a;

                },[]);
                  break;               
              default: 

            }

          }
          catch(e) {
            console.log(e);
          }

          return option;

        });

        emitter.emit('change order of headers');

    },

    replaceRequestData: function(req,data){
        //console.log(data.toString('ascii'));
      return data;
    },

    replaceResponseStatusCode: function(req,res,statusCode){
        return statusCode;
    },

    replaceResponseHeader: function(req,res,header) {
      // console.log(res);
      // console.log(header);
      // var header_file = __dirname + "/cache/" + md5(req.headers.host + req.url) + "_" + req.headers['dl_cache_proxy_country'] + "_" + req.headers['dl_cache_proxy_device_type'] + "_" + req.headers['dl_cache_proxy_device'] + "_" + req.headers['dl_cache_proxy_browser'] + "_" + "header";

      // if(req.headers['dl_cache_proxy_base_url'] != req.url) {
          //fs.writeFile(header_file, JSON.stringify(header), function (err) {
              //console.log("Failed to create header cache");
          //});
      // }
      // 
      
      // try {
      //   var newRes  = (JSON.parse(JSON.stringify(header)));
      //   for (var headers in newRes) {
      //     if (headers == 'accept-ranges' && newRes[headers] == 'none') {
      //       delete newRes[headers];
      //     }
      //   }

      //   header = newRes;
      // }
      // catch(e) {
      //   console.log(e);
      // }
        
      return header;
    },

    replaceServerResDataAsync: function(req,res,serverResData,callback){
        var body_file = __dirname + "/cache/" + md5(req.headers.host + req.url) + "_" + req.headers['dl_cache_proxy_country'] + "_" + req.headers['dl_cache_proxy_device_type'] + "_" + req.headers['dl_cache_proxy_device'] + "_" + req.headers['dl_cache_proxy_browser'] + "_" + "body";
        var bodyContents = new Buffer(serverResData).toString();
        if(bodyContents.length + JSON.stringify(res.headers).length > 1200) {
            fs.writeFile(body_file, bodyContents, function (err) {
                //console.log("Failed to create body cache");
            });
        }
        callback(serverResData);
    },

    pauseBeforeSendingResponse : function(req,res){
        return 0;
    },

    shouldInterceptHttpsReq :function(req){
        return true;
    }
};
