// job.js
"use strict";
var util = require('util');
var http = require('http');
var https = require("https");

var Job = function Job() {
  var cc = this; 
  cc.initiateRequest =  function(options,nextCall) {

  	var req = ( /443/.test(options.port) ? https : http).request(options, function (res) {
		  var chunks = [];

		  res.on("data", function (chunk) {
		    chunks.push(chunk);
		  });

		  res.on("end", function () {
		    var body = Buffer.concat(chunks);
		    cc.emit('requestCompleted',res.headers,nextCall);
		  });
	  });

	  req.end();

	  req.on('error', function(e) {
		  console.error(e);
		});
  };
};

Job.prototype.adCallUtils : function() {

	return {
    clientId : 3665752689669427,
    slotId : 1995171792,
    siteUrl : 'http://www.mandola.in/adsense',
    protocol : 'http:',
    host : 'www.mandola.in',
    adUnits : {}
	}
};

require('events').EventEmitter.defaultMaxListeners = Infinity; 
util.inherits(Job, require('events').EventEmitter);
module.exports = Job;